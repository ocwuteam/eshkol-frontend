function nmImgFill() {
	if ($(".js-img").length) {
		var img_count = $(".js-img").length;
		for (var i = 0; i < img_count; i++) {
			var jthis = $(".js-img").eq(i);
			var jimg = jthis.children("img");
			jimg.removeAttr("style");
			if (jimg.outerWidth() < jthis.outerWidth()) {
				jimg.css("width", "100%").css("height", "auto")
			} else {
				if (jimg.outerHeight() < jthis.outerHeight()) {
					jimg.css("width", "auto").css("height", "100%")
				}
			}
		}
	}
}

$(document).ready(function () {
	if($('.commitment__slider').length){
		$('.commitment__slider').each(function(){
			var this_slider = $(this);
			var val = this_slider.data('val');
			this_slider.find('.commitment__slider-val').css('width', val+'%');
			this_slider.find('.commitment__slider-val span').text(val+'%');
		})
	}
	sliderCon();
	nmImgFill();
	$('.logo_slider').slick({
		rtl: true,
		infinite: true,
		adaptiveHeight: true,
		slidesToShow: 7,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 5
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 630,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
	$('.category__slider').slick({
		rtl: true,
		infinite: true,
		adaptiveHeight: true,
		slidesToShow: 1
	});
	$('.category__slider2').slick({
		rtl: true,
		infinite: true,
		adaptiveHeight: true,
		slidesToShow: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});
	$('.slick-arrow').addClass('no_back_change');
	
});

$(window).resize(function () {
	nmImgFill();
	sliderCon();
});

$(document).ready(function () {

	$('.tab__head').click(function(event){
		event.preventDefault();
		$('.tab__head').removeClass('is-active');
		$(this).addClass('is-active');
		var tab_num = $(this).index();
		$('.tab__body').removeClass('is-active');
		$('.tab__body').eq(tab_num).addClass('is-active');
	});
	$('.is-modal').click(function(){
		var h_h = $(window).scrollTop();
		$('.modal__bg').fadeIn(300);
		$('.modal__wrap').css('top', (h_h + 50) + 'px').slideDown(300);
	});
	$('.modal__bg, .modal__close').click(function(){
		$('.modal__bg').fadeOut(300);
		$('.modal__wrap').slideUp(300);
	});

});

function sliderCon(){ 
 
	if($(window).innerWidth() <= 480){ 
		if(!($('.commitments').hasClass('slick-slider'))){ 
			$('.commitments').slick({ 
				rtl: true, 
				infinite: true, 
				slidesToShow: 1, 
			}) 
		} 
	} else{ 
		if($('.commitments').hasClass('slick-slider')){ 
			$('.commitments').slick('unslick'); 
		} 
	} 
}