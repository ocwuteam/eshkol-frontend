var pr_timer;

$(document).ready(function(){
	$('.header-search').on('click', function(){
		clearTimeout(pr_timer);

		pr_timer = setTimeout(function(){
			$('header').addClass('search_active');
			if($('header').hasClass('search_active')){
				$('.header-search').attr('tabindex', '-1');
			} else if(!($('header').hasClass('search_active'))){
				$('.header-search').attr('tabindex', '1');
			}
		}, 150)
	})
	$('.header-accecibility').on('click', function(){
		if($('header').hasClass('search_active')){
			$('header').removeClass('search_active');
			$('.search-input input').val('')
		}
	})


	$('.header-nav').on('click', function(){
		clearTimeout(pr_timer);
		pr_timer = setTimeout(function(){
			$('.header-nav').toggleClass('active');
		}, 150)
	})

	//close toggle on body click
	$(document).on("click", "*", function (e) {
		e.stopPropagation();
		if (window.innerWidth <= 1024) {
			if (!($(this).closest(".header-nav").length)) {
				$('.header-nav').removeClass('active');
			}
		}
	})

})