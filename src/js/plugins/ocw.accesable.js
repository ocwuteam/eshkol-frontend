//create accesable menu
jQuery(document).ready(function($) {
	var root = document.location.hostname;
	console.log(root);
	var accesable_menu = 
		'<div id="accability_toolbar" class="no_back_change">' +
			'<h5 class="skipArea data-key=\'0\'"><a href="javascript:void(0)" data-scroll_to=".header-login" class="the_skip">הגעת לתפריט הנגישות, לחץ ENTER על מנת לדלג על התפריט.</a></h5>' + 

			'<button id="show_accability_menu" class="no_back_change" title="הצג תפריט נגישות"> ' + 
				' הצג תפריט נגישות ' +
				/*'<img src="js/ocw_accesable_src/img/int.png" alt="הצג תפריט נגישות" class="acc-icon no_back_change">' + */
				/*'<svg version="1.1" id="icon-accability" class="no_back_change" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 46.3 49.1" xml:space="preserve"><path d="M11,21.1c-0.8-0.8-2-0.9-2.9-0.2C5.9,22.7,1.9,26.6,2,32.6c0.2,11.1,10.5,14.7,15.6,14.5 c4.2,0,8.5-2,10.7-4.1c0.4-0.4,0.7-0.7,1-1.1c0.7-0.8,0.6-2-0.1-2.8c0,0,0,0,0,0c-0.8-0.9-2.1-1-3-0.2c-0.2,0.1-0.3,0.2-0.3,0.3 c-3.8,2.9-6.1,3.2-8.2,3.2c-7.3,0.1-11-5.8-11-9.8c0.1-3.7,2.4-6.3,4.4-8.2C12,23.5,12,22.1,11,21.1C11.1,21.2,11,21.1,11,21.1z"/><path d="M17,32c-1.3,0-2.3-1-2.3-2.3V14.3c0-1,0.8-1.8,1.8-1.8H17c1,0,1.8,0.7,1.8,1.7l0.6,7h9 c0.7,0,1.4,0.6,1.4,1.3l0,1c0,0.7-0.6,1.3-1.3,1.3L20,24.6l0.2,3.1H32c1.8,0,2.5,0.8,3,2.3l3.8,9.7l2.7-1c1.4-0.5,2.9,0.5,3,1.9 l0,0.1c0.1,1.2-0.7,2.3-1.9,2.7L38,45l-0.1,0c-1.1,0.4-2.3-0.2-2.8-1.2l-0.6-1.4l-3.7-10.2L17,32L17,32z"/></g><circle cx="19.1" cy="6.6" r="5.2"/></svg>' +*/
			'</button>' +

			/*
			'<button id="magnifier" class="no_back_change" title="הגדל טקסט - זכוכית מגדלת">' +
				'<i class="no_back_change nagish nagish-search-plus"></i>' +
				'<span class="sr-only">הגדל טקסט - זכוכית מגדלת</span>' +
			'</button>' +
			*/
		
			'<button id="enlarge_text" class="no_text_change no_back_change font_s" title="הגדל טקסט">' +
				'א' +
			'</button>' +

			'<button id="reset_text" class="no_text_change no_back_change font_s active" title="איפוס טקסט">' +
				'א' +
			'</button>' +

			'<button id="dec_text" class="no_text_change no_back_change font_s" title="הקטן טקסט">' +
				'א' +
			'</button>' +

			'<button id="light_bac" class="back_s no_back_change" title="רקע בהיר">' +
				'רקע בהיר' +
			'</button>' +

			'<button id="reg_bac" class="back_s active no_back_change" title="איפוס רקע">' +
				'איפוס רקע' +
			'</button>' +

			'<button id="dark_bac" class="back_s no_back_change" title="רקע כהה">' +
				'רקע כהה' +
			'</button>' +

			'<button id="links_b" class="no_back_change" title="הדגשת קישורים">' +
				'<svg version="1.1" class="no_back_change" id="icon-links" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 94.8 94.7" xml:space="preserve"><g><path d="M97.4,72.9c0,4.6-1.6,8.6-4.9,11.8l-8.5,8.5c-3.2,3.2-7.1,4.8-11.8,4.8c-4.7,0-8.6-1.6-11.8-4.9l-12-12 c-3.2-3.2-4.8-7.1-4.8-11.8c0-4.8,1.7-8.8,5.1-12.1l-5.1-5.1c-3.3,3.4-7.4,5.1-12.1,5.1c-4.6,0-8.6-1.6-11.8-4.9L7.5,40.2 C4.3,37,2.6,33,2.6,28.4c0-4.6,1.6-8.6,4.9-11.8l8.5-8.5c3.2-3.2,7.1-4.8,11.8-4.8c4.7,0,8.6,1.6,11.8,4.9l12,12 c3.2,3.2,4.8,7.1,4.8,11.8c0,4.8-1.7,8.8-5.1,12.1l5.1,5.1c3.3-3.4,7.4-5.1,12.1-5.1c4.6,0,8.6,1.6,11.8,4.9l12.1,12.1 C95.7,64.4,97.4,68.3,97.4,72.9z M45.4,32c0-1.5-0.5-2.9-1.6-3.9l-12-12c-1.1-1.1-2.4-1.6-3.9-1.6c-1.5,0-2.8,0.5-3.9,1.6l-8.5,8.5 c-1.1,1.1-1.6,2.4-1.6,3.9c0,1.5,0.5,2.9,1.6,3.9l12.1,12.1c1,1,2.4,1.6,3.9,1.6c1.6,0,3-0.6,4.2-1.8c-0.1-0.1-0.5-0.5-1.1-1.1 c-0.6-0.6-1-1-1.2-1.2c-0.2-0.2-0.5-0.6-0.9-1.1c-0.4-0.5-0.6-1-0.8-1.5c-0.1-0.5-0.2-1-0.2-1.6c0-1.5,0.5-2.9,1.6-3.9 c1.1-1.1,2.4-1.6,3.9-1.6c0.6,0,1.1,0.1,1.6,0.2c0.5,0.1,1,0.4,1.5,0.8c0.5,0.4,0.9,0.7,1.1,0.9c0.2,0.2,0.6,0.6,1.2,1.2 c0.6,0.6,1,1,1.1,1.1C44.8,35.1,45.4,33.7,45.4,32z M86.2,72.9c0-1.5-0.5-2.9-1.6-3.9L72.5,56.9c-1.1-1.1-2.4-1.6-3.9-1.6 c-1.6,0-3,0.6-4.2,1.9c0.1,0.1,0.5,0.5,1.1,1.1c0.6,0.6,1,1,1.2,1.2c0.2,0.2,0.5,0.6,0.9,1.1c0.4,0.5,0.6,1,0.8,1.5 c0.1,0.5,0.2,1,0.2,1.6c0,1.5-0.5,2.9-1.6,3.9c-1.1,1.1-2.4,1.6-3.9,1.6c-0.6,0-1.1-0.1-1.6-0.2c-0.5-0.1-1-0.4-1.5-0.8 c-0.5-0.4-0.9-0.7-1.1-0.9c-0.2-0.2-0.6-0.6-1.2-1.2c-0.6-0.6-1-1-1.1-1.1c-1.3,1.2-1.9,2.6-1.9,4.2c0,1.5,0.5,2.9,1.6,3.9l12,12 c1,1,2.4,1.6,3.9,1.6c1.5,0,2.9-0.5,3.9-1.5l8.5-8.5C85.7,75.8,86.2,74.5,86.2,72.9z"/></g></svg>' +
				'<span class="sr-only">הדגשת קישורים</span>' +
			'</button>' +

			'<button id="font_change_ro_arial" class="no_back_change" title="פונט נגיש">' +
				'<svg version="1.1" class="no_back_change" id="icon-font" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 73.2 70.5" xml:space="preserve"><g><path d="M46.7,47.4H20.1l-4.7,10.9c-1.2,2.7-1.7,4.7-1.7,6c0,1,0.5,2,1.5,2.8s3.2,1.3,6.5,1.5v1.9H0v-1.9 c2.9-0.5,4.7-1.2,5.6-2c1.7-1.6,3.6-4.9,5.7-9.9L35.5,0h1.8l24,57.3c1.9,4.6,3.7,7.6,5.3,9s3.8,2.1,6.6,2.3v1.9H45.9v-1.9 c2.7-0.1,4.6-0.6,5.6-1.4c1-0.8,1.4-1.7,1.4-2.8c0-1.5-0.7-3.8-2-7.1L46.7,47.4z M45.3,43.6L33.6,15.8l-12,27.8H45.3z"/></g></svg>' +
				'<span class="sr-only">פונט נגיש</span>' +
			'</button>' +
		
			'<button class="access_button_style no_back_change" data-href="">משוב</button>' +
		
			'<button class="access_button_style no_back_change" data-href="">הצהרת נגישות</button>' +
		'</div>';
	
	$("body").prepend(accesable_menu)
})

//user actions
jQuery(document).ready(function($) {
	$('body').on('click', '.access_button_style[data-href]', function () {
		window.location = $(this).attr('data-href');
	})

	$('body').on('click', '.access_button_style[data-scroll_to]', function () {
		scroll_to_elem($($(this).attr('data-scroll_to')));
		$($(this).attr('data-scroll_to')).focus();
	})

	$('body').on('click', '#show_accability_menu', function () {
		$(this).closest("#accability_toolbar").toggleClass("open")
	})
	
	$('body').on('mousedown', '#show_accability_menu', function () {
		$(this).addClass('clicked');
	})

	$('body').on('mouseup', '#show_accability_menu', function () {
		$(this).removeClass('clicked');
	})
	
	/*
	$('body').on('click', '#show_accability_menu', function () {
		if ($(this).hasClass('active')) {
			$('#accability_toolbar > button:not(#show_accability_menu),.access_button_style').hide();
		} else {
			$('#accability_toolbar > button:not(#show_accability_menu),.access_button_style').show();
		}
	})
	*/

	$('body').on('focus', '#show_accability_menu', function () {
		if (!$(this).hasClass('clicked')) {
			$(this).click()
		}
	})

	$('body').on('keydown', '#accability_toolbar button:last-child', function (ev) {
		if (ev.which == 9) {
			$(this).removeClass('active');
			$('#accability_toolbar > button:not(#show_accability_menu),.access_button_style').hide();
		}
	})

	$('body').on('click', '#accability_toolbar > button,.access_button_style', function () {
		if ($(this).hasClass('font_s') || $(this).hasClass('back_s')) {
			return
		}

		if ($(this).hasClass('active')) {
			$(this).removeClass('active')
		} else {
			$(this).addClass('active')
		}
	})

	$('body').on('click', '#magnifier', function () {
		if ($(this).hasClass('active')) {
			$('body').addClass('maginify')
		} else {
			$('body').removeClass('maginify')
		}
	})

	$('body').on('mouseenter', 'p,a,h1,h2,h3,h4,h6', function () {
		if (!$('body').hasClass('maginify')) {
			return
		} else {
			$(this).append('<div class="apended_glas_mag">' + $(this).html() + '</div>')
		}
	})

	$('body').on('mouseleave', 'p,a,h1,h2,h3,h4,h6', function () {
		if (!$('body').hasClass('maginify')) {
			return
		} else {
			$('.apended_glas_mag').remove()
		}
	})

	$('body').on('click', '.font_s', function () {
		if (!$(this).hasClass('active')) {
			$('.font_s').removeClass('active');
			$(this).addClass('active');

			var tid = $(this).attr('id');

			$('body').removeClass('enlarge_text');
			$('body').removeClass('dec_text');
			$('body').addClass(tid);
		} else {
			//$('.font_s').removeClass('active');
		}
	})

	$('body').on('click', '.back_s', function () {

		if (!$(this).hasClass('active')) {
			$('.back_s').removeClass('active');
			$(this).addClass('active');

			var tid = $(this).attr('id');

			$('body').removeClass('light_bac');
			$('body').removeClass('dark_bac');
			$('body').addClass(tid);
		} else {
		}
	})

	$('body').on('click', '#links_b', function () {
		if ($(this).hasClass('active')) {
			$('body').addClass('links_b');
		} else {
			$('body').removeClass('links_b');
		}
	})

	$('body').on('click', '#font_change_ro_arial', function () {
		if ($(this).hasClass('active')) {
			$('body').addClass('font_change_ro_arial');
		} else {
			$('body').removeClass('font_change_ro_arial');
		}
	})

	$('body').prepend($('.move_to_top_of_body').html());
	$('.move_to_top_of_body').hide();

	$('body').on('focus', '.the_skip', function () {
		$(this).parent().css('top', 'auto');
	})

	$('body').on('blur', '.the_skip', function () {
		$(this).parent().css('top', '-9999px');

	})

	$('body').on('click', '.the_skip', function () {
		var scroll_to = $(this).attr('data-scroll_to');

		if (typeof scroll_to !== typeof undefined && scroll_to !== false) {
			if ($(scroll_to).length) {
				$(scroll_to).focus();
				scroll_to_elem($(scroll_to));
			} else {
				return false;
			}
			return false
		} else {

			var data_key = $(this).parent().attr('data-key');

			if (typeof data_key !== typeof undefined && data_key !== false) {
				var next_key = parseInt(data_key) + 1;
			} else {
				var next_key = 1;
			}

			if ($('.skipArea[data-key="' + next_key + '"]').length > 0) {
				var new_scroll = $('.skipArea[data-key="' + next_key + '"] a');
			} else {
				var new_scroll = $('.skipArea[data-key="0"] a');
			}

			scroll_to_elem(new_scroll.parent().parent());
			new_scroll.focus();
			return false
		}
		return false
	})

	$('img').each(function () {
		var attr = $(this).attr('alt');
		if (typeof attr === typeof undefined || attr === false) {
			$(this).attr('alt', '');
		}
	})
})

function scroll_to_elem(element) {
	jQuery("html, body").animate({
		scrollTop: element.offset().top - 40
	}, 500)
}